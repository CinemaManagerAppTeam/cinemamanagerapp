using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using CinemaManager.Domain.Model;
using CinemaManager.WebApi.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CinemaManager.WebApi.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<CinemaManager.Domain.Data.CinemaManagerData>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(CinemaManager.Domain.Data.CinemaManagerData context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //


            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            RoleManager.Create(new IdentityRole("Manager"));
            RoleManager.Create(new IdentityRole("Seller"));
            RoleManager.Create(new IdentityRole("Viewer"));

            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var user = new ApplicationUser()
            {
                UserName = "franek",
                Email = "franek@gmail.com",
                EmailConfirmed = true,
                Name = "Fraciszek",
                Surname = "Franowski"
            };

            var result = manager.Create(user, "SuperStrongPassword123!");
            if (result.Succeeded)
            {
                var currentUser = manager.FindByName(user.UserName);

                var roleresult = manager.AddToRole(currentUser.Id, "Manager");
            }

            var employee = new ApplicationUser()
            {
                UserName = "mareczek",
                Email = "mareczek@gmail.com",
                EmailConfirmed = true,
                Name = "Marek",
                Surname = "Marecki"
            };

            result = manager.Create(employee, "SuperStrongPassword123!");
            if (result.Succeeded)
            {
                var currentUser = manager.FindByName(employee.UserName);

                var roleresult = manager.AddToRole(currentUser.Id, "Seller");
            }

            var viewer = new ApplicationUser()
            {
                UserName = "jdoe",
                Email = "jdoe@gmail.com",
                EmailConfirmed = true,
                Name = "John",
                Surname = "Doe"
            };

            result = manager.Create(viewer, "SuperStrongPassword123!");
            if (result.Succeeded)
            {
                var currentUser = manager.FindByName(viewer.UserName);

                var roleresult = manager.AddToRole(currentUser.Id, "Viewer");
            }

               Room room_seled = new Room() { Name = "Seledynowy" };
            context.Rooms.Add(room_seled);
            context.Rooms.Add(new Room() { Name = "Pistacjowy" });
            context.Rooms.Add(new Room() { Name = "Chabrowy" });


            Movie m_django = new Movie()
            {
                Title = "Django",
                Description = "Dobry Film",
                Duration = new TimeSpan(1, 30, 00)
            };
            context.Movies.Add(m_django);
            context.Movies.Add(new Movie()
            {
                Title = "Harry Potter",
                Description = "film o magii",
                Duration = new TimeSpan(1, 50, 00)
            });
            context.Movies.Add(new Movie()
            {
                Title = "W�adca Pier�cieni",
                Description = "Film oscarowy",
                Duration = new TimeSpan(2, 30, 00)
            });
            context.Movies.Add(new Movie()
            {
                Title = "Inception",
                Description = "We need to go deeper.",
                Duration = new TimeSpan(2, 28, 00)
            });
            context.Movies.Add(new Movie()
            {
                Title = "Pulp Fiction",
                Description = "Die motherfuckers!",
                Duration = new TimeSpan(2, 34, 00)
            });
            
            context.Screenings.Add(new Screening() { Movie = m_django, Room = room_seled, DateTimeStart = new DateTime(2015, 11, 16, 15, 30, 00) });
            context.Screenings.Add(new Screening() { Movie = m_django, Room = room_seled, DateTimeStart = new DateTime(2016, 11, 16, 15, 30, 00) });

            context.SaveChanges();

        }
    }
}
