﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CinemaManager.Domain.Data;
using CinemaManager.Domain.Model;
using CinemaManager.Shared.Dtos;
using CinemaManager.WebApi.Filters;

namespace CinemaManager.WebApi.Controllers
{
    [RequireHttps]
    [RoutePrefix("api/screenings")]
    public class ScreeningsController : ApiController
    {

        private readonly ScreeningAssembler _screeningAssembler = new ScreeningAssembler();
        private readonly CinemaManagerRepository _repo = new CinemaManagerRepository(new CinemaManagerData());
        private readonly Validation validation = new Validation();


        public ScreeningsController()
        {
        }


        
        public IHttpActionResult Get()
        {
            CinemaManagerRepository _repo2 = new CinemaManagerRepository(new CinemaManagerData());
            var scr = _repo2.GetScreenings();
            return scr == null ? (IHttpActionResult)this.NotFound()
                : this.Ok(_screeningAssembler.ToScreeningDetailsDto(scr));
        }

      
        [Route("{dt:datetime:regex(\\d{4}-\\d{2}-\\d{2})}")]
        public IHttpActionResult GetForDate(DateTime dt)
        {
            var screenings = _repo.GetScreeningsForDate(dt);
            return screenings == null ? (IHttpActionResult)this.NotFound()
                : this.Ok(_screeningAssembler.ToScreeningDetailsDto(screenings));
        }

        
        [Route("{roomId:int}")]
        public IHttpActionResult GetForRoomId(int roomId)
        {
            var screenings = _repo.GetScreeningsForRoomId(roomId);
            return screenings == null ? (IHttpActionResult)this.NotFound()
                : this.Ok(_screeningAssembler.ToScreeningDetailsDto(screenings));
        }

        [Authorize(Roles ="Manager")]
        public IHttpActionResult Post([FromBody] ScreeningDetailsDto newScreeningDto)
        {
            //TODO: Validate new student data - this is test implementation only

            var message = validation.ValidateAddScreening(newScreeningDto.Title, newScreeningDto.DateTimeStart, newScreeningDto.RoomName);
            if (message == null)
            {
                _repo.AddScreening(newScreeningDto.Title, newScreeningDto.DateTimeStart, newScreeningDto.RoomName);
            }

            return this.Ok(message);
        }
    }
}