﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CinemaManager.Domain.Data;
using CinemaManager.Domain.Model;
using CinemaManager.Shared.Dtos;
using CinemaManager.WebApi.Filters;

namespace CinemaManager.WebApi.Controllers
{
    [RequireHttps]
    public class TicketsController : ApiController
    {
        private CinemaManagerRepository cinemaManagerRepository = new CinemaManagerRepository(new CinemaManagerData());

        [Authorize]
        [Route("~/api/Screenings/{id:int}/Tickets")]
        public async Task<IHttpActionResult> GetTicketForScreening(int id)
        {
            var screening = cinemaManagerRepository.GetScreening(id);
            if (screening == null)
                return NotFound();
            var result = cinemaManagerRepository.GetTicketsReservedForScreening(screening.Movie.Title,
                screening.DateTimeStart,
                screening.Room.Name).Select(i => new TicketDetailsDto()
                {
                    SeatNumber = i
                }).ToList();
            return (IHttpActionResult)Ok(result);
            //return (result.Count != 0) ? (IHttpActionResult)Ok(result) : NotFound();
        }


        [Authorize]
        public async Task<IHttpActionResult> Post([FromBody] TicketDetailsDto ticketDetails)
        {
            var screning = cinemaManagerRepository.GetScreening(ticketDetails.ScreeningId);
            if (screning == null) return NotFound();
            cinemaManagerRepository.ReserveTickets(new List<int>() { ticketDetails.SeatNumber }, screning.Movie.Title, screning.DateTimeStart, screning.Room.Name);
            return Ok(ticketDetails.SeatNumber);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

            }
            base.Dispose(disposing);
        }

    }
}