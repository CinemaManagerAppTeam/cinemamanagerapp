﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CinemaManager.Domain.Data;
using CinemaManager.Domain.Model;
using CinemaManager.Shared.Dtos;
using CinemaManager.WebApi.Filters;

namespace CinemaManager.WebApi.Controllers
{
    [RequireHttps]
    public class RoomsController : ApiController
    {
        private CinemaManagerData db = new CinemaManagerData();

        // GET: api/Rooms
        
        public IEnumerable<RoomInfoDto> GetRooms()
        {
            return db.Rooms.Select((room => new RoomInfoDto() { RoomName = room.Name }));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}