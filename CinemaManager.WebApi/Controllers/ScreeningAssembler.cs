﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CinemaManager.Shared.Dtos;
using CinemaManager.Domain.Model;

namespace CinemaManager.WebApi.Controllers
{
    public class ScreeningAssembler
    {
        public ScreeningDetailsDto ToScreeningDetailsDto(Screening screening)
        {
            return new ScreeningDetailsDto()
            {
                ScreeningId = screening.Id,
                DateTimeStart = screening.DateTimeStart,

                MovieId = screening.Movie.Id,
                Description = screening.Movie.Description,
                Duration = screening.Movie.Duration,
                Title = screening.Movie.Title,

                RoomId = screening.Room.Id,
                RoomName = screening.Room.Name
            };
        }

        public IList<ScreeningDetailsDto> ToScreeningDetailsDto(IList<Screening> screenings)
        {
            return screenings.Select(ToScreeningDetailsDto).ToList();
        }


        public Screening ToNewEntity(ScreeningDetailsDto dto)
        {
            var scr = new Screening()
            {
                DateTimeStart = dto.DateTimeStart



            };
            //TODO: Introduce login generation strategy, validate login uniqueness etc.; this is test implementation only

            return scr;
        }
    }
}