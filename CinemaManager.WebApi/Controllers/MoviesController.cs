﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CinemaManager.Domain.Data;
using CinemaManager.Domain.Model;
using CinemaManager.Shared.Dtos;
using CinemaManager.WebApi.Filters;

namespace CinemaManager.WebApi.Controllers
{
    //[RoutePrefix("api/movies")]
    [RequireHttps]
    public class MoviesController : ApiController
    {
        private readonly MovieAssembler _movieAssembler = new MovieAssembler();
        private readonly CinemaManagerRepository _repo = new CinemaManagerRepository(new CinemaManagerData());
        private readonly Validation validation = new Validation();

        public MoviesController()
        {

        }

      
        public IHttpActionResult Get()
        {
            var movies = _repo.GetMovies();
            return movies == null ? (IHttpActionResult)this.NotFound()
                : this.Ok(_movieAssembler.ToMovieDetailsDto(movies));
        }
        

        [Authorize(Roles ="Manager")]
        public IHttpActionResult Post([FromBody] MovieDetailsDto newMovieDto)
        {
            //TODO: Validate new student data - this is test implementation only

            var message = validation.ValidateAddMovie(newMovieDto.Title, newMovieDto.Description, newMovieDto.Duration);
            if(message==null)
            {
                Movie movie = _movieAssembler.ToNewEntity(newMovieDto);
                _repo.AddMovie(movie.Title, movie.Description, movie.Duration);
            }

            return this.Ok(message);
        }
    }
}