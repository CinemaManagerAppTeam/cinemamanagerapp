﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CinemaManager.WebApi.Controllers
{
    public class HomeController : Controller
    {
        [Filters.RequireHttps]
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return Redirect(Url.Content("/app/index.html"));
        }
    }
}
