﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CinemaManager.Shared.Dtos;
using CinemaManager.Domain.Model;

namespace CinemaManager.WebApi.Controllers
{
    public class MovieAssembler
    {   
        
        public MovieDetailsDto ToMovieDetailsDto(Movie movie)
        {
            return new MovieDetailsDto()
            {
                Title = movie.Title,
                Description = movie.Description,
                Duration = movie.Duration.ToString(),
                MovieId = movie.Id

            };
        }

        public IList<MovieDetailsDto> ToMovieDetailsDto(IList<Movie> movies)
        {
            return movies.Select(ToMovieDetailsDto).ToList();
        }
        

        public Movie ToNewEntity(MovieDetailsDto dto)
        {
            var movie = new Movie()
            {
                Description = dto.Description,
                Duration = TimeSpan.Parse(dto.Duration),
                Title = dto.Title
            };
            //TODO: Introduce login generation strategy, validate login uniqueness etc.; this is test implementation only

            return movie;
        }

    }
}