var cinemaManagerApp = angular.module('cinemaManagerApp', ['ngRoute', 'LocalStorageModule']);

cinemaManagerApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'views/index.html',
            controller: 'IndexController'
        }).when('/movies', {
            templateUrl: 'views/movies.html',
            controller: 'MoviesController'
        }).when('/screenings', {
            templateUrl: 'views/screenings.html',
            controller: 'ScreeningsController'
        }).when('/contact', {
            templateUrl: 'views/contact.html',
            controller: 'ContactController'
        }).when('/tickets', {
            templateUrl: 'views/tickets.html',
            controller: 'TicketsController'
        }).when('/register', {
            templateUrl: 'views/register.html',
            controller: 'RegisterController'
        }).when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginController',
        }).when('/select_seat', {
            templateUrl: 'views/select_seat.html',
            controller: 'SelectSeatController'
        }).otherwise({
            redirectTo: '/'
        });
    }]);

cinemaManagerApp.factory('authInterceptorService', ['$q', '$location', 'localStorageService', function ($q, $location, localStorageService) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
    }

    var _responseError = function (rejection) {
        if (rejection.status === 401) {
            $location.path('/login');
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);

cinemaManagerApp.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

cinemaManagerApp.controller('IndexController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {
    $scope.logOut = function () {
        authService.logOut();
        $location.path('/home');
    }

    $scope.authentication = authService.authentication;
}]);

cinemaManagerApp.controller('MoviesController', function ($scope, $http) {
    $scope.movies = [
        {
            title: "Django",
            shortDescription: "The D is silent",
            description: "In 1858, in Texas, the former German dentist Dr. King Schultz meets the slave Django in a lonely road while being transported by the slavers Speck Brothers. He asks if Django knows the Brittle Brothers and with the affirmative, he buys Django for him. Then Dr. Schultz tells that he is a bounty hunter chasing John, Ellis and Roger Brittle and proposes a deal to Django: if he helps him, he would give his freedom, a horse and US$ 75.00 for him. Django accepts the deal and Dr. Schultz trains him to be his deputy. They kill the brothers in Daughtray and Django tells that he would use the money to buy the freedom of his wife Broomhilda, who is a slave that speaks German. Dr. Schultz proposes another deal to Django: if he teams-up with him during the winter, he would give one-third of the rewards and help him to rescue Broomhilda. Django accepts his new deal and they become friends. After the winter, Dr. Schultz goes to Gatlinburgh and learns that Broomhilda was sold to the ruthless Calvin Candie von Shaft, who lives in the Candyland Farm, in Mississippi. Dr. Schultz plots a scheme with Django to lure Calvin and rescue Broomhilda from him. But his cruel minion Stephen is not easily fooled. ",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            date: "2016-01-23T10:15:30+01:00",
            duration: "2:45",
            genres: "Western",
            stars: 5
        },
        {
            title: "Inception",
            shortDescription: "We need to go deeper",
            description: "Dom Cobb is a skilled thief, the absolute best in the dangerous art of extraction, stealing valuable secrets from deep within the subconscious during the dream state, when the mind is at its most vulnerable. Cobb's rare ability has made him a coveted player in this treacherous new world of corporate espionage, but it has also made him an international fugitive and cost him everything he has ever loved. Now Cobb is being offered a chance at redemption. One last job could give him his life back but only if he can accomplish the impossible-inception. Instead of the perfect heist, Cobb and his team of specialists have to pull off the reverse: their task is not to steal an idea but to plant one. If they succeed, it could be the perfect crime. But no amount of careful planning or expertise can prepare the team for the dangerous enemy that seems to predict their every move. An enemy that only Cobb could have seen coming. ",
            poster: "http://www.filmofilia.com/wp-content/uploads/2010/04/Inception_poster.jpg",
            date: "2016-01-23T10:15:30+01:00",
            duration: "2:28",
            genres: "Action, Mystery, Sci-Fi",
            stars: 5
        },
        {
            title: "Pulp Fiction",
            shortDescription: "Die Motherfuckers!",
            description: "Jules Winnfield and Vincent Vega are two hitmen who are out to retrieve a suitcase stolen from their employer, mob boss Marsellus Wallace. Wallace has also asked Vincent to take his wife Mia out a few days later when Wallace himself will be out of town. Butch Coolidge is an aging boxer who is paid by Wallace to lose his next fight. The lives of these seemingly unrelated people are woven together comprising of a series of funny, bizarre and uncalled-for incidents. ",
            poster: "https://www.movieposter.com/posters/archive/main/105/MPW-52844",
            date: "2016-01-23T10:15:30+01:00",
            duration: "2:34",
            genres: "Crime, Drama",
            stars: 5
        },
        {
            title: "Django",
            shortDescription: "",
            description: "In 1858, in Texas, the former German dentist Dr. King Schultz meets the slave Django in a lonely road while being transported by the slavers Speck Brothers. He asks if Django knows the Brittle Brothers and with the affirmative, he buys Django for him. Then Dr. Schultz tells that he is a bounty hunter chasing John, Ellis and Roger Brittle and proposes a deal to Django: if he helps him, he would give his freedom, a horse and US$ 75.00 for him. Django accepts the deal and Dr. Schultz trains him to be his deputy. They kill the brothers in Daughtray and Django tells that he would use the money to buy the freedom of his wife Broomhilda, who is a slave that speaks German. Dr. Schultz proposes another deal to Django: if he teams-up with him during the winter, he would give one-third of the rewards and help him to rescue Broomhilda. Django accepts his new deal and they become friends. After the winter, Dr. Schultz goes to Gatlinburgh and learns that Broomhilda was sold to the ruthless Calvin Candie von Shaft, who lives in the Candyland Farm, in Mississippi. Dr. Schultz plots a scheme with Django to lure Calvin and rescue Broomhilda from him. But his cruel minion Stephen is not easily fooled. ",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            date: "2016-01-23T10:15:30+01:00",
            duration: "2:45",
            genres: "Western",
            stars: 5
        },
        {
            title: "Django",
            shortDescription: "",
            description: "In 1858, in Texas, the former German dentist Dr. King Schultz meets the slave Django in a lonely road while being transported by the slavers Speck Brothers. He asks if Django knows the Brittle Brothers and with the affirmative, he buys Django for him. Then Dr. Schultz tells that he is a bounty hunter chasing John, Ellis and Roger Brittle and proposes a deal to Django: if he helps him, he would give his freedom, a horse and US$ 75.00 for him. Django accepts the deal and Dr. Schultz trains him to be his deputy. They kill the brothers in Daughtray and Django tells that he would use the money to buy the freedom of his wife Broomhilda, who is a slave that speaks German. Dr. Schultz proposes another deal to Django: if he teams-up with him during the winter, he would give one-third of the rewards and help him to rescue Broomhilda. Django accepts his new deal and they become friends. After the winter, Dr. Schultz goes to Gatlinburgh and learns that Broomhilda was sold to the ruthless Calvin Candie von Shaft, who lives in the Candyland Farm, in Mississippi. Dr. Schultz plots a scheme with Django to lure Calvin and rescue Broomhilda from him. But his cruel minion Stephen is not easily fooled. ",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            date: "2016-01-23T10:15:30+01:00",
            duration: "2:45",
            genres: "Western",
            stars: 5
        },
        {
            title: "Django",
            shortDescription: "",
            description: "In 1858, in Texas, the former German dentist Dr. King Schultz meets the slave Django in a lonely road while being transported by the slavers Speck Brothers. He asks if Django knows the Brittle Brothers and with the affirmative, he buys Django for him. Then Dr. Schultz tells that he is a bounty hunter chasing John, Ellis and Roger Brittle and proposes a deal to Django: if he helps him, he would give his freedom, a horse and US$ 75.00 for him. Django accepts the deal and Dr. Schultz trains him to be his deputy. They kill the brothers in Daughtray and Django tells that he would use the money to buy the freedom of his wife Broomhilda, who is a slave that speaks German. Dr. Schultz proposes another deal to Django: if he teams-up with him during the winter, he would give one-third of the rewards and help him to rescue Broomhilda. Django accepts his new deal and they become friends. After the winter, Dr. Schultz goes to Gatlinburgh and learns that Broomhilda was sold to the ruthless Calvin Candie von Shaft, who lives in the Candyland Farm, in Mississippi. Dr. Schultz plots a scheme with Django to lure Calvin and rescue Broomhilda from him. But his cruel minion Stephen is not easily fooled. ",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            date: "2016-01-23T10:15:30+01:00",
            duration: "2:45",
            genres: "Western",
            stars: 5
        },
        {
            title: "Django",
            shortDescription: "",
            description: "In 1858, in Texas, the former German dentist Dr. King Schultz meets the slave Django in a lonely road while being transported by the slavers Speck Brothers. He asks if Django knows the Brittle Brothers and with the affirmative, he buys Django for him. Then Dr. Schultz tells that he is a bounty hunter chasing John, Ellis and Roger Brittle and proposes a deal to Django: if he helps him, he would give his freedom, a horse and US$ 75.00 for him. Django accepts the deal and Dr. Schultz trains him to be his deputy. They kill the brothers in Daughtray and Django tells that he would use the money to buy the freedom of his wife Broomhilda, who is a slave that speaks German. Dr. Schultz proposes another deal to Django: if he teams-up with him during the winter, he would give one-third of the rewards and help him to rescue Broomhilda. Django accepts his new deal and they become friends. After the winter, Dr. Schultz goes to Gatlinburgh and learns that Broomhilda was sold to the ruthless Calvin Candie von Shaft, who lives in the Candyland Farm, in Mississippi. Dr. Schultz plots a scheme with Django to lure Calvin and rescue Broomhilda from him. But his cruel minion Stephen is not easily fooled. ",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            date: "2016-01-23T10:15:30+01:00",
            duration: "2:45",
            genres: "Western",
            stars: 5
        }
    ];
});

cinemaManagerApp.controller('ScreeningsController', function ($scope, $http) {
    $scope.screenings = [
        {
            title: "Django",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Pulp Fiction",
            poster: "https://www.movieposter.com/posters/archive/main/105/MPW-52844",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Inception",
            poster: "http://www.filmofilia.com/wp-content/uploads/2010/04/Inception_poster.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Django",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Django",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Pulp Fiction",
            poster: "https://www.movieposter.com/posters/archive/main/105/MPW-52844",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Inception",
            poster: "http://www.filmofilia.com/wp-content/uploads/2010/04/Inception_poster.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Django",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Django",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Pulp Fiction",
            poster: "https://www.movieposter.com/posters/archive/main/105/MPW-52844",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Inception",
            poster: "http://www.filmofilia.com/wp-content/uploads/2010/04/Inception_poster.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Django",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Django",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Pulp Fiction",
            poster: "https://www.movieposter.com/posters/archive/main/105/MPW-52844",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Inception",
            poster: "http://www.filmofilia.com/wp-content/uploads/2010/04/Inception_poster.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        },
        {
            title: "Django",
            poster: "http://chic-type.com/blog/wp-content/uploads/2013/01/django_unchained_8.jpg",
            datetimes: ["2016-01-23T10:15:30+01:00", "2016-01-23T14:30:30+01:00", "2016-01-23T18:15:30+01:00", "2016-01-23T10:20:00+01:00", "2016-01-23T22:15:30+01:00"]
        }
    ];
});

cinemaManagerApp.controller('TicketsController', function ($scope, $http) {
    $scope.tickets = [
        {
            title: "Django",
            name: "John",
            surname: "Doe",
            datetime: "2016-01-23T10:15:30+01:00",
            seat: 156
        },
        {
            title: "Inception",
            name: "John",
            surname: "Doe",
            datetime: "2016-01-23T10:15:30+01:00",
            seat: 157
        },
        {
            title: "Pulp Fiction",
            name: "John",
            surname: "Doe",
            datetime: "2016-01-23T10:15:30+01:00",
            seat: 200
        },
        {
            title: "Django",
            name: "John",
            surname: "Doe",
            datetime: "2016-01-23T10:15:30+01:00",
            seat: 77
        },
        {
            title: "Django",
            name: "John",
            surname: "Doe",
            datetime: "2016-01-23T10:15:30+01:00",
            seat: 169
        },
        {
            title: "Django",
            name: "John",
            surname: "Doe",
            datetime: "2016-01-23T10:15:30+01:00",
            seat: 148
        },
        {
            title: "Django",
            name: "John",
            surname: "Doe",
            datetime: "2016-01-23T10:15:30+01:00",
            seat: 65
        },
        {
            title: "Django",
            name: "John",
            surname: "Doe",
            datetime: "2016-01-23T10:15:30+01:00",
            seat: 205
        }
    ];
});

cinemaManagerApp.controller('RegisterController', ['$scope', '$location', '$timeout', 'authService', function ($scope, $location, $timeout, authService) {
    $scope.savedSuccessfully = false;
    $scope.message = "";

    $scope.registration = {
        Email: "",
        password: "",
        confirmPassword: ""
    };

    $scope.signUp = function () {

        authService.saveRegistration($scope.registration).then(function (response) {

                $scope.savedSuccessfully = true;
                $scope.message = "User has been registered successfully, you will be redicted to login page in 2 seconds.";
                startTimer();

            },
            function (response) {
                var errors = [];
                for (var key in response.data.modelState) {
                    for (var i = 0; i < response.data.modelState[key].length; i++) {
                        errors.push(response.data.modelState[key][i]);
                    }
                }
                $scope.message = "Failed to register user due to:" + errors.join(' ');
            });
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $location.path('/login');
        }, 2000);
    }
}]);

cinemaManagerApp.controller('LoginController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {
    $scope.loginData = {
        userName: "",
        password: ""
    };

    $scope.message = "";

    $scope.login = function () {

        authService.login($scope.loginData).then(function (response) {

                $location.path('/#index');

            },
            function (err) {
                $scope.message = err.error_description;
            });
    };
}]);

cinemaManagerApp.controller('ContactController', function ($scope, $http) {
    (function initialize() {
        var mapCanvas = document.getElementById('map');
        var mapOptions = {
            center: new google.maps.LatLng(44.5403, -78.5463),
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
    })()
});

cinemaManagerApp.controller('SelectSeatController', function ($scope, $http) {

});

cinemaManagerApp.run(['authService', function (authService) {
    authService.fillAuthData();
}]);

cinemaManagerApp.factory('authService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {

    var serviceBase = 'https://localhost:44300/';
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: ""
    };

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
            return response;
        });

    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(serviceBase + 'token', data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function (response) {

            localStorageService.set('authorizationData', {token: response.access_token, userName: loginData.userName});

            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = "";

    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }

    }

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;

    return authServiceFactory;
}]);

