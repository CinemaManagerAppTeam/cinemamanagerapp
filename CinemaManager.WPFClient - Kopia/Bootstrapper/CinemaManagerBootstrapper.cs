﻿using System.Windows;
using Caliburn.Micro;
using CinemaManager.WebApiWPFClient.ViewModels;

namespace CinemaManager.WebApiWPFClient.Bootstrapper
{
    public class CinemaManagerBootstrapper : BootstrapperBase
    {
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainWindowViewModel>();
        }
    }
}