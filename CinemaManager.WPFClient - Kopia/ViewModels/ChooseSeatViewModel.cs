﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using Caliburn.Micro;
using CinemaManager.Shared.Dtos;
using CinemaManager.WebApiWPFClient.Infrastracture;
using CinemaManager.WebApiWPFClient.Services;

namespace CinemaManager.WebApiWPFClient.ViewModels
{
    public class ChooseSeatViewModel : ViewModelBase
    {
        private TicketsProxy _ticketsProxy;

        public ChooseSeatViewModel(MainWindowViewModel viewModel, Film chosenScreening)
        {
            this.masterViewModel = viewModel;
            this._ticketsProxy = new TicketsProxy(masterViewModel.Token.AccessToken);
            ChosenScreening = chosenScreening;
            GetSeats();
            SelectSeatCommand = new RelayCommand(SelectSeat, o => CanSelectSeat);
        }

        private async void GetSeats()
        {
            int seatCount = 12 * 24;
            Seat[] list = new Seat[12 * 24];
            for (int i = 0; i < seatCount; i++)
            {
                list[i] = new Seat() { SeatInfo = SeatInfo.Free, SeatNo = i };
            }

            var reservedSeats = await _ticketsProxy.GetTicketsForScreening(ChosenScreening.ScreeningId);
            if (reservedSeats != null)
                foreach (var reservedSeat in reservedSeats.Select(dto => dto.SeatNumber))
                {
                    list[reservedSeat] = new Seat() { SeatInfo = SeatInfo.Reserved, SeatNo = reservedSeat };
                }
            Seats = new ObservableCollection<Seat>(list);
            selectedSeats = new List<int>();
        }

        public Film ChosenScreening { get; set; }

        private List<int> selectedSeats;

        private ObservableCollection<Seat> seats;

        public ObservableCollection<Seat> Seats
        {
            get { return seats; }
            set
            {
                seats = value;
                NotifyOfPropertyChange(() => Seats);
            }
        }

        public async void LoadEnterViewerData()
        {
            foreach (var seatNumber in selectedSeats)
            {
                await _ticketsProxy.Post(new TicketDetailsDto()
                {
                    ScreeningId = ChosenScreening.ScreeningId,
                    SeatNumber = seatNumber,
                    UserLogin = masterViewModel.Token.Username
                });
            }
            masterViewModel.LoadedView = new MenuViewModel(masterViewModel);
        }

        public bool CanLoadEnterViewerData
        {
            get { return true; }
        }

        public void SelectSeat(object id)
        {
            int seatId = (int)id;
            switch (Seats[seatId].SeatInfo)
            {
                case SeatInfo.Free:
                    Seats[seatId].SeatInfo = SeatInfo.Selected;
                    selectedSeats.Add(seatId);
                    break;
                case SeatInfo.Selected:
                    Seats[seatId].SeatInfo = SeatInfo.Free;
                    selectedSeats.Remove(seatId);
                    break;
            }
            NotifyOfPropertyChange(() => Seats);
        }

        private ICommand selectSeatCommand;
        public ICommand SelectSeatCommand
        {
            get
            {
                return selectSeatCommand;
            }
            set
            {
                selectSeatCommand = value;
                NotifyOfPropertyChange(() => selectSeatCommand);
            }
        }

        public bool CanSelectSeat
        {
            get { return true; }
        }
    }

    public class Seat : PropertyChangedBase
    {
        private SeatInfo _seatInfo;
        private int _seatNo;

        public SeatInfo SeatInfo
        {
            get { return _seatInfo; }
            set
            {
                _seatInfo = value;
                NotifyOfPropertyChange(() => SeatInfo);
            }
        }

        public int SeatNo
        {
            get { return _seatNo; }
            set
            {
                _seatNo = value;
                NotifyOfPropertyChange(() => SeatNo);
            }
        }
    }

    public enum SeatInfo
    {
        Selected,
        Free,
        Reserved,
        Bought
    }
}