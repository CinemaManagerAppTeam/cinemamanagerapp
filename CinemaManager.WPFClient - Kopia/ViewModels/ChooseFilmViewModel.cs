﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Windows;
using Caliburn.Micro;
using CinemaManager.WebApiWPFClient.Infrastracture;
using CinemaManager.WebApiWPFClient.Services;
using System.Threading.Tasks;
using CinemaManager.Shared.Dtos;



namespace CinemaManager.WebApiWPFClient.ViewModels

{
    public class ChooseFilmViewModel : ViewModelBase
    {
        public ChooseFilmViewModel(MainWindowViewModel masterViewModel)
        {
            this._screeningProxy = new ScreeningsProxy(masterViewModel.Token.AccessToken);
            this.masterViewModel = masterViewModel;
            Films = new ObservableCollection<Film>();
            Date = DateTime.Now;
            this.clientValidation = new ClientValidation();

                
        }

        private readonly ClientValidation clientValidation;

        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                LoadScreeningsWhenDateChanged();
                NotifyOfPropertyChange(() => Date);
            }
        }

        private async void LoadScreeningsWhenDateChanged()
        {
            Films.Clear();
            var screenings = await _screeningProxy.Get(Date);

            foreach (var screening in screenings)
            {
                films.Add(new Film() { Description = screening.Description, Title = screening.Title, Hour = screening.DateTimeStart, Room = screening.RoomName, ScreeningId = screening.ScreeningId });
            }
        }

        private ObservableCollection<Film> films;
        private DateTime _date;

        public ObservableCollection<Film> Films
        {
            get { return films; }
            set
            {
                films = value;
                NotifyOfPropertyChange(() => Films);
            }
        }

        public Film SelectedFilm { get; set; }


        public void LoadChooseSeat()
        {
            if (SelectedFilm == null)
            {
                MessageBox.Show("Please choose one film!");
                return;
            }

            
            var message = clientValidation.ValidateChooseFilm(SelectedFilm.Hour,masterViewModel.EmployeeRoles.First());
            if (message == null)
            {
                masterViewModel.LoadedView = new ChooseSeatViewModel(masterViewModel, SelectedFilm);
            }
            else
            {
                MessageBox.Show(message);
            }

        }


        private readonly ScreeningsProxy _screeningProxy;




        public bool CanLoadChooseSeat
        {
            get
            {
                //if (SelectedFilm == null)
                //{
                //    MessageBox.Show("Please select film!");
                //    return false;
                //}
                //else return true;
                return true;
            }
        }

    }


    public class Film
    {
        public string Title { get; set; }
        public DateTime Hour { get; set; }
        public string Description { get; set; }
        public string Room { get; set; }
        public int ScreeningId { get; set; }
    }

}