﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using CinemaManager.WebApiWPFClient.Infrastracture;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using CinemaManager.Shared.Dtos;
using CinemaManager.Shared.Dtos.CinemaManager.Shared.Dtos;

namespace CinemaManager.WebApiWPFClient.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
          //  CinemaManagerRepository = new CinemaManagerRepository(new CinemaManagerData());
            LoadedView = new MenuViewModel(this);
            EmployeeLogin = null;
            EmployeeRoles = null;
        }

        private ViewModelBase loadedView;
        private string _employeeLogin;
        public TokenModel Token;

        public ViewModelBase LoadedView
        {
            get { return loadedView; }
            set
            {
                loadedView = value;
                NotifyOfPropertyChange(() => LoadedView);
            }
        }

        public string EmployeeLogin
        {
            get { return _employeeLogin; }
            set
            {
                _employeeLogin = value;
                NotifyOfPropertyChange(() => EmployeeLogin);
            }
        }

        private IEnumerable<string> employeeRoles;

        public IEnumerable<string> EmployeeRoles
        {
            get { return employeeRoles; }
            set
            {
                employeeRoles = value;
                NotifyOfPropertyChange(() => EmployeeRoles);
            }
        }


        //public CinemaManagerRepository CinemaManagerRepository { get; set; }

        public async void Login(object window)
        {
            if (Token != null)
            {
                Token = null;
                EmployeeRoles = null;
            }
            else
            {
                var result = await (window as MetroWindow).ShowLoginAsync("Login", "Please input your credentials");
                if (result == null) return;

                using (HttpClient httpClient = new HttpClient() { BaseAddress = new Uri("https://localhost:44300/") })
                {
                    var responseMessage = await httpClient.PostAsync("/Token", new FormUrlEncodedContent(
                        new[]
                        {
                            new KeyValuePair<string, string>("grant_type", "password"),
                            new KeyValuePair<string, string>("username", result.Username),
                            new KeyValuePair<string, string>("password", result.Password),
                        }
                        ));
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        Token = await responseMessage.Content.ReadAsAsync<TokenModel>();
                        if (Token != null)
                        {
                            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {Token.AccessToken}");
                            responseMessage = await httpClient.GetAsync("/api/Account/UserInfo");
                            var responseContent = await responseMessage.Content.ReadAsStringAsync();
                           // MessageBox.Show(responseContent);
                            EmployeeRoles =
                                (await responseMessage.Content.ReadAsAsync<AccountViewModels.UserInfoViewModel>()).Roles;
                        }
                    }
                }

            }
            LoadedView = new MenuViewModel(this);
        }

        public bool CanLogin
        {
            get { return true; }
        }

        public void LoadMenu()
        {
            this.LoadedView = new MenuViewModel(this);
        }

        public bool CanLoadMenu
        {
            get { return true; }
        }
    }
}
