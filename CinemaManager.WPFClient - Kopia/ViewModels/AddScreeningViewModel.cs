﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Windows;
using Caliburn.Micro;
using CinemaManager.WebApiWPFClient.Infrastracture;
using CinemaManager.WebApiWPFClient.Services;
using CinemaManager.Shared.Dtos;
using System.Threading.Tasks;
using CinemaManager.WebApiWPFClient.Properties;

namespace CinemaManager.WebApiWPFClient.ViewModels

{
    public class AddScreeningViewModel : ViewModelBase
    {
        public AddScreeningViewModel(MainWindowViewModel viewModel)
        {

            DateTime = DateTime.Now;
            masterViewModel = viewModel;
            this._screeningsProxy = new ScreeningsProxy(masterViewModel.Token.AccessToken);

            LoadMovieTitles();
        }

        private async void LoadMovieTitles()
        {

            var moviesProxy = new MoviesProxy(masterViewModel.Token.AccessToken);
            Films = new ObservableCollection<string>((await moviesProxy.Get()).Select(dto => dto.Title));

            using (HttpClient httpClient = new HttpClient() { BaseAddress = new Uri(Settings.Default.BaseServicesUrl) })
            {
                var roomRespponse = await httpClient.GetAsync("api/Rooms");
                if (roomRespponse.IsSuccessStatusCode)
                {
                    var rooms = await roomRespponse.Content.ReadAsAsync<IEnumerable<RoomInfoDto>>(new[] { new JsonMediaTypeFormatter() });
                    if (rooms != null)
                        Rooms = new ObservableCollection<string>(rooms.Select(info => info.RoomName));
                }
            }

        }

        private ObservableCollection<string> films;

        public ObservableCollection<string> Films
        {
            get { return films; }
            set
            {
                films = value;
                NotifyOfPropertyChange(() => Films);
            }
        }

        private ObservableCollection<string> rooms;

        public ObservableCollection<string> Rooms
        {
            get { return rooms; }
            set
            {
                rooms = value;
                NotifyOfPropertyChange(() => Rooms);
            }
        }

        private string title;

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }

        private string roomName;
        public string RoomName
        {
            get { return roomName; }
            set
            {
                roomName = value;
                NotifyOfPropertyChange(() => RoomName);
            }
        }

        private DateTime dateTime;
        public DateTime DateTime
        {
            get { return dateTime; }
            set
            {
                dateTime = value;
                NotifyOfPropertyChange(() => DateTime);
            }
        }

        public async void AddScreening()
        {           
            string message = await this.AddScr(Title, DateTime, RoomName);

            if (message == null)
                masterViewModel.LoadMenu();
            else MessageBox.Show(message);
        }

        private readonly ScreeningsProxy _screeningsProxy;
        

        public async Task<string> AddScr(string title, DateTime datetimestart, string roomName)
        {
            return await _screeningsProxy.Post(new ScreeningDetailsDto() { Title = title, DateTimeStart = datetimestart, RoomName = roomName });
        }

        public bool CanAddScreening
        {
            get { return true; }
        }

    }
}