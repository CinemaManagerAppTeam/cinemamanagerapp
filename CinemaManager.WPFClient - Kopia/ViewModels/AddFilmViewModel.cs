﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using CinemaManager.WebApiWPFClient.Infrastracture;
using Microsoft.Win32;
using CinemaManager.WebApiWPFClient.Services;
using CinemaManager.Shared.Dtos;

namespace CinemaManager.WebApiWPFClient.ViewModels
{
    class AddFilmViewModel : ViewModelBase
    {
        public AddFilmViewModel(MainWindowViewModel masterViewModel)
        {
            Title = "Django";
            Description = "Nice movie.";
            Duration = "1:59:59";
            this.masterViewModel = masterViewModel;
            this._moviesProxy = new MoviesProxy(masterViewModel.Token.AccessToken);
        }
        

        private String title;

        public String Title
        {
            get { return title; }
            set
            {
                title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                NotifyOfPropertyChange(() => Description);
            }
        }

        private string duration;

        public string Duration
        {
            get { return duration; }
            set
            {
                duration = value;
                NotifyOfPropertyChange(() => Duration);
            }
        }

        
        public async void Save()
        {         
                string message = await this.AddMovie(Title, Description, Duration);

                if(message==null)
                    masterViewModel.LoadMenu();
                else MessageBox.Show(message);


        }


        private readonly MoviesProxy _moviesProxy;

        /*
        public async Task GetAllMovies()
        {
            IList<MovieDetailsDto> filmy = await new MoviesProxy().Get();
            int a = 5;
        }*/

        public async Task<string> AddMovie(string title, string description, string duration)
        {
           return await _moviesProxy.Post(new MovieDetailsDto() { Description = description, Duration = duration, Title = title });
        }

        public bool CanSave
        {
            get {

                return true;
                }
        }
    }
}
