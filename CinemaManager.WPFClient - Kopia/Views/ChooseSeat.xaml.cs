﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using CinemaManager.WebApiWPFClient.ViewModels;
using MahApps.Metro.Controls;

namespace CinemaManager.WebApiWPFClient.Views
{
    /// <summary>
    /// Interaction logic for ChooseSeat.xaml
    /// </summary>
    public partial class ChooseSeat : UserControl
    {
        public ChooseSeat()
        {
            InitializeComponent();
        }
    }
}
