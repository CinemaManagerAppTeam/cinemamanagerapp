﻿using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace CinemaManager.WebApiWPFClient.Views
{
    /// <summary>
    /// Interaction logic for EnterViewerData.xaml
    /// </summary>
    public partial class EnterViewerData : UserControl
    {
        public EnterViewerData()
        {
            InitializeComponent();
        }
    }
}
