﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CinemaManager.Shared.Dtos;

namespace CinemaManager.WebApiWPFClient.Services
{
    public class TicketsProxy : WebApiProxy
    {
        public TicketsProxy(string token) : base("", token)
        {

        }

        public async Task<IList<TicketDetailsDto>> GetTicketsForScreening(int id)
        {
            return await this.Get<IList<TicketDetailsDto>>($"Screenings/{id}/Tickets");
        }

        public async Task<int> Post(TicketDetailsDto ticketDetails)
        {
            return await Post<TicketDetailsDto, int>("Tickets", ticketDetails);
        }
    }
}