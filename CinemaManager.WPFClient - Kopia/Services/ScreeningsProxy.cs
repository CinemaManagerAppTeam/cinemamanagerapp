﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CinemaManager.Shared.Dtos;

namespace CinemaManager.WebApiWPFClient.Services
{
    public class ScreeningsProxy : WebApiProxy
    {
        public ScreeningsProxy(string token) : base("Screenings", token)
        {

        }

        public async Task<IList<ScreeningDetailsDto>> Get()
        {
            return await this.Get<IList<ScreeningDetailsDto>>("");
        }

        public async Task<IList<ScreeningDetailsDto>> Get(DateTime date)
        {
            return await this.Get<IList<ScreeningDetailsDto>>(date.ToShortDateString());
        }


        public async Task<string> Post(ScreeningDetailsDto screeningDto)
        {
            return await this.Post<ScreeningDetailsDto, string>(screeningDto);
        }
    }
}
