﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CinemaManager.Shared.Dtos;

namespace CinemaManager.WebApiWPFClient.Services
{
    public class MoviesProxy : WebApiProxy
    {
        public MoviesProxy(string token) : base("Movies", token)
        {

        }

        public async Task<List<MovieDetailsDto>> Get()
        {
            return await this.Get<List<MovieDetailsDto>>("");
        }

        public async Task<string> Post(MovieDetailsDto movieDto)
        {
            return await this.Post<MovieDetailsDto, string>(movieDto);
        }

        /*
        public async Task<IList<StudentDetailsDto>> Search(string searchPhrase)
        {
            return await this.Get<IList<StudentDetailsDto>>(string.Concat("Search/", HttpUtility.UrlEncode(searchPhrase)));
        }
        */
    }
}
