﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaManager.WebApiWPFClient.Infrastracture
{
    public class ClientValidation
    {
        public string ValidateChooseFilm(DateTime datetime, string role)
        {
            StringBuilder sb = new StringBuilder();
            if (datetime < DateTime.Now)
                sb.Append("Cannot choose film - start time of film already passed!");
            if (role == "Seller" && datetime - DateTime.Now > new TimeSpan(7, 0, 0, 0))
                sb.Append("Cannot choose film - for a seller no more than 7 days ahead!");

            var message = sb.ToString();
            if (message.Length > 0) return message;
            else return null;
        }
    }
}
