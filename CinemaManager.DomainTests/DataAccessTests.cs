﻿using System;
using System.Collections.Generic;
using System.Linq;
using CinemaManager.Domain.Data;
using CinemaManager.Domain.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CinemaManager.Domain.Tests
{
    [TestClass]
    public class DataAccessTests
    {
        [TestMethod]
        public void InsertAndRetrieveNewMovieWithScreeningTest()
        {
            using (CinemaManagerData cinemaManagerContext = new CinemaManagerData())
            {
                Movie movie = new Movie()
                {
                    Description = "Nice Movie",
                    Duration = new TimeSpan(1, 50, 0),
                    Title = "Django222"
                };
                Screening screening = new Screening()
                {
                    DateTimeStart = new DateTime(2015, 11, 11, 18, 00, 00),
                    Movie = movie,

                };
                Room room = new Room() { Name = "AAA" };
                movie.Screenings = new List<Screening>() { screening };
                screening.Room = room;
                cinemaManagerContext.Movies.Add(movie);
                cinemaManagerContext.Screenings.Add(screening);
                cinemaManagerContext.SaveChanges();
                Movie django = cinemaManagerContext.Movies.FirstOrDefault(m => m.Title == "Django2");
                var screeningOfDjango = django.Screenings.FirstOrDefault();
                Assert.AreEqual(django.Title, "Django222");
                Assert.AreEqual(django.Description, "Nice Movie");
                Assert.AreEqual(django.Duration, new TimeSpan(1, 50, 0));
                Assert.AreEqual(screeningOfDjango.DateTimeStart, new DateTime(2015, 11, 11, 18, 00, 00));
            }
        }
    }
}
