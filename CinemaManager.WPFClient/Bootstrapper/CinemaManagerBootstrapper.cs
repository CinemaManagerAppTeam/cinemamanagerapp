﻿using System.Windows;
using Caliburn.Micro;
using CinemaManager.WPFClient.ViewModels;

namespace CinemaManager.WPFClient.Bootstrapper
{
    public class CinemaManagerBootstrapper : BootstrapperBase
    {
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainWindowViewModel>();
        }
    }
}