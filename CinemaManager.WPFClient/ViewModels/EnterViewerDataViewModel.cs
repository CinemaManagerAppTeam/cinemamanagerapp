﻿using System.Collections.Generic;
using System.Windows.Documents;
using Caliburn.Micro;
using CinemaManager.WPFClient.Infrastracture;

namespace CinemaManager.WPFClient.ViewModels
{
    public class EnterViewerDataViewModel : ViewModelBase
    {
        private string _name;
        private string _surname;
        private string _email;

        public EnterViewerDataViewModel(MainWindowViewModel mainWindowViewModel, Film chosenScreening, List<int> selectedSeats)
        {
            this.masterViewModel = mainWindowViewModel;
            SelectedSeats = selectedSeats;
            ChosenScreening = chosenScreening;
        }

        private Film ChosenScreening { get; set; }
        private List<int> SelectedSeats { get; set; }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public string Surname
        {
            get { return _surname; }
            set
            {
                _surname = value;
                NotifyOfPropertyChange(() => Surname);
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                NotifyOfPropertyChange(() => Email);
            }
        }

        public void LoadMenu()
        {
            masterViewModel.LoadMenu();
        }

        public bool CanLoadMenu
        {
            get { return true; }
        }
    }
}