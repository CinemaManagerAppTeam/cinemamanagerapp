﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using Caliburn.Micro;
using CinemaManager.Domain.Data;
using CinemaManager.WPFClient.Infrastracture;
namespace CinemaManager.WPFClient.ViewModels
{
    public class AddScreeningViewModel : ViewModelBase
    {
        public AddScreeningViewModel(MainWindowViewModel viewModel)
        {
            Films = new ObservableCollection<string>(viewModel.CinemaManagerRepository.GetMoviesTitles());
       
            Rooms = new ObservableCollection<string>(viewModel.CinemaManagerRepository.GetRoomsNames());
            DateTime = DateTime.Now;
            masterViewModel = viewModel;
        }

        private ObservableCollection<string> films;

        public ObservableCollection<string> Films
        {
            get { return films; }
            set
            {
                films = value;
                NotifyOfPropertyChange(() => Films);
            }
        }

        private ObservableCollection<string> rooms;

        public ObservableCollection<string> Rooms
        {
            get { return rooms; }
            set
            {
                rooms = value;
                NotifyOfPropertyChange(() => Rooms);
            }
        }

        private string title;

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }

        private string roomName;
        public string RoomName
        {
            get { return roomName; }
            set
            {
                roomName = value;
                NotifyOfPropertyChange(() => RoomName);
            }
        }

        private DateTime dateTime;
        public DateTime DateTime
        {
            get { return dateTime; }
            set
            {
                dateTime = value;
                NotifyOfPropertyChange(() => DateTime);
            }
        }

        public void AddScreening()
        {

            var message = new Validation().ValidateAddScreening(Title, DateTime, RoomName);
            if (message == null)
            {
                masterViewModel.CinemaManagerRepository.AddScreening(Title, DateTime, RoomName);
                masterViewModel.LoadMenu();
            }
            else
            {
                MessageBox.Show(message);
            }

        }

        public bool CanAddScreening
        {
            get { return true; }
        }

    }
}