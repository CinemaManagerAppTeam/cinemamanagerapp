﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using CinemaManager.Domain.Data;
using CinemaManager.WPFClient.Infrastracture;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace CinemaManager.WPFClient.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            CinemaManagerRepository = new CinemaManagerRepository(new CinemaManagerData());
            LoadedView = new MenuViewModel(this);
            EmployeeLogin = null;
            EmployeeRole = null;
        }

        private ViewModelBase loadedView;
        private string _employeeLogin;

        public ViewModelBase LoadedView
        {
            get { return loadedView; }
            set
            {
                loadedView = value;
                NotifyOfPropertyChange(() => LoadedView);
            }
        }

        public string EmployeeLogin
        {
            get { return _employeeLogin; }
            set
            {
                _employeeLogin = value;
                NotifyOfPropertyChange(() => EmployeeLogin);
            }
        }

        private string employeeRole;

        public string EmployeeRole
        {
            get { return employeeRole; }
            set
            {
                employeeRole = value;
                NotifyOfPropertyChange(() => EmployeeRole);
            }
        }


        public CinemaManagerRepository CinemaManagerRepository { get; set; }

        public async void Login(object window)
        {
            if (EmployeeLogin != null)
            {
                EmployeeLogin = null;
                EmployeeRole = null;
            }
            else
            {
                var result = await (window as MetroWindow).ShowLoginAsync("Login", "Please input your credentials");
                if (result == null) return;
              /*  if (CinemaManagerRepository.Login(result.Username, result.Password))
                {
                    EmployeeLogin = result.Username;
                    EmployeeRole = CinemaManagerRepository.GetRoleFromLogin(EmployeeLogin);
                }*/
            }
            LoadedView = new MenuViewModel(this);
        }

        public bool CanLogin
        {
            get { return true; }
        }

        public void LoadMenu()
        {
            this.LoadedView = new MenuViewModel(this);
        }

        public bool CanLoadMenu
        {
            get { return true; }
        }

    }
}
