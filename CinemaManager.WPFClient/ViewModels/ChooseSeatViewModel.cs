﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using Caliburn.Micro;
using CinemaManager.WPFClient.Infrastracture;

namespace CinemaManager.WPFClient.ViewModels
{
    public class ChooseSeatViewModel : ViewModelBase
    {
        public ChooseSeatViewModel(MainWindowViewModel viewModel, Film chosenScreening)
        {
            this.masterViewModel = viewModel;
            int seatCount = 12 * 24;
            Seat[] list = new Seat[12 * 24];
            for (int i = 0; i < seatCount; i++)
            {
                list[i] = new Seat() { SeatInfo = SeatInfo.Free, SeatNo = i };
            }

            var reservedSeats = viewModel.CinemaManagerRepository.GetTicketsReservedForScreening(chosenScreening.Title,
                chosenScreening.Hour, chosenScreening.Room);
            if (reservedSeats != null)
                foreach (var reservedSeat in reservedSeats)
                {
                    list[reservedSeat] = new Seat() { SeatInfo = SeatInfo.Reserved, SeatNo = reservedSeat };
                }
            Seats = new ObservableCollection<Seat>(list);
            selectedSeats = new List<int>();
            SelectSeatCommand = new RelayCommand(SelectSeat, o => CanSelectSeat);
            ChosenScreening = chosenScreening;
        }

        public Film ChosenScreening { get; set; }

        private List<int> selectedSeats;

        private ObservableCollection<Seat> seats;

        public ObservableCollection<Seat> Seats
        {
            get { return seats; }
            set
            {
                seats = value;
                NotifyOfPropertyChange(() => Seats);
            }
        }

        public void LoadEnterViewerData()
        {
            masterViewModel.CinemaManagerRepository.ReserveTickets(selectedSeats, ChosenScreening.Title, ChosenScreening.Hour, ChosenScreening.Room);
            masterViewModel.LoadedView = new MenuViewModel(masterViewModel);
        }

        public bool CanLoadEnterViewerData
        {
            get { return true; }
        }

        public void SelectSeat(object id)
        {
            int seatId = (int)id;
            switch (Seats[seatId].SeatInfo)
            {
                case SeatInfo.Free:
                    Seats[seatId].SeatInfo = SeatInfo.Selected;
                    selectedSeats.Add(seatId);
                    break;
                case SeatInfo.Selected:
                    Seats[seatId].SeatInfo = SeatInfo.Free;
                    selectedSeats.Remove(seatId);
                    break;
            }
            NotifyOfPropertyChange(() => Seats);
        }

        private ICommand selectSeatCommand;
        public ICommand SelectSeatCommand
        {
            get
            {
                return selectSeatCommand;
            }
            set
            {
                selectSeatCommand = value;
                NotifyOfPropertyChange(() => selectSeatCommand);
            }
        }

        public bool CanSelectSeat
        {
            get { return true; }
        }
    }

    public class Seat : PropertyChangedBase
    {
        private SeatInfo _seatInfo;
        private int _seatNo;

        public SeatInfo SeatInfo
        {
            get { return _seatInfo; }
            set
            {
                _seatInfo = value;
                NotifyOfPropertyChange(() => SeatInfo);
            }
        }

        public int SeatNo
        {
            get { return _seatNo; }
            set
            {
                _seatNo = value;
                NotifyOfPropertyChange(() => SeatNo);
            }
        }
    }

    public enum SeatInfo
    {
        Selected,
        Free,
        Reserved,
        Bought
    }
}