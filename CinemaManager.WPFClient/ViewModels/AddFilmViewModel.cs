﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using CinemaManager.Domain.Data;
using CinemaManager.WPFClient.Infrastracture;
using Microsoft.Win32;

namespace CinemaManager.WPFClient.ViewModels
{
    class AddFilmViewModel : ViewModelBase
    {
        public AddFilmViewModel(MainWindowViewModel masterViewModel)
        {
            Title = "Django";
            Description = "Nice movie.";
            Duration = "1:59:59";
            this.masterViewModel = masterViewModel;
        }
        
        private String title;

        public String Title
        {
            get { return title; }
            set
            {
                title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                NotifyOfPropertyChange(() => Description);
            }
        }

        private string duration;

        public string Duration
        {
            get { return duration; }
            set
            {
                duration = value;
                NotifyOfPropertyChange(() => Duration);
            }
        }

        public void Save()
        {
           
            var message = new Validation().ValidateAddMovie(Title, Description, Duration);
            if (message==null)
            {
                masterViewModel.CinemaManagerRepository.AddMovie(Title, Description, TimeSpan.Parse(Duration));
                masterViewModel.LoadMenu();
            }
            else
            {
                MessageBox.Show(message);
            }
        }

        public bool CanSave
        {
            get {

                return true;
                }
        }
    }
}
