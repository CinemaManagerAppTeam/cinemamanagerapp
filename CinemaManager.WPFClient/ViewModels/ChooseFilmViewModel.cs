﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using Caliburn.Micro;
using CinemaManager.WPFClient.Infrastracture;
using CinemaManager.Domain.Data;

namespace CinemaManager.WPFClient.ViewModels

{
    public class ChooseFilmViewModel : ViewModelBase
    {
        public ChooseFilmViewModel(MainWindowViewModel masterViewModel)
        {
            this.masterViewModel = masterViewModel;
            Films = new ObservableCollection<Film>();
            Date = DateTime.Now;
        }

        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                LoadScreeningsWhenDateChanged();
                NotifyOfPropertyChange(() => Date);
            }
        }

        private void LoadScreeningsWhenDateChanged()
        {
            var screenings = masterViewModel.CinemaManagerRepository.GetScreeningsForDate(Date);
            Films.Clear();
            foreach (var screening in screenings)
            {
                films.Add(new Film() { Description = screening.Movie.Description, Title = screening.Movie.Title, Hour = screening.DateTimeStart, Room = screening.Room.Name });
            }
        }

        private ObservableCollection<Film> films;
        private DateTime _date;

        public ObservableCollection<Film> Films
        {
            get { return films; }
            set
            {
                films = value;
                NotifyOfPropertyChange(() => Films);
            }
        }

        public Film SelectedFilm { get; set; }

        public void LoadChooseSeat()
        {
                if(SelectedFilm==null)
                {
                    MessageBox.Show("Please choose one film!");
                    return;
                }

                var message = new Validation().ValidateChooseFilm(Date, masterViewModel.EmployeeRole);
                if (message == null)
                {
                    masterViewModel.LoadedView = new ChooseSeatViewModel(masterViewModel, SelectedFilm);
                }
                else
                {
                    MessageBox.Show(message);
                }
        }

        public bool CanLoadChooseSeat
        {
            get
            {
                //if (SelectedFilm == null)
                //{
                //    MessageBox.Show("Please select film!");
                //    return false;
                //}
                //else return true;
                return true;
            }
        }

    }


        public class Film
        {
            public string Title { get; set; }
            public DateTime Hour { get; set; }
            public string Description { get; set; }
            public string Room { get; set; }
        }
    
}