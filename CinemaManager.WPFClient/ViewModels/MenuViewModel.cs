﻿using System.Windows.Input;
using Caliburn.Micro;
using CinemaManager.WPFClient.Infrastracture;

namespace CinemaManager.WPFClient.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {
        public MenuViewModel(MainWindowViewModel masterViewModel)
        {
            this.masterViewModel = masterViewModel;
            NotifyOfPropertyChange(() => CanLoadCheckAvailability);
        }


        RelayCommand checkAvailabilityCommand;
        public ICommand CheckAvailabilityCommand
        {
            get
            {
                if (checkAvailabilityCommand == null)
                {
                    checkAvailabilityCommand = new RelayCommand(param => this.LoadCheckAvailability(),
                        param => this.CanLoadCheckAvailability);
                }
                return checkAvailabilityCommand;
            }
        }

        public void LoadCheckAvailability()
        {
            masterViewModel.LoadedView = new ChooseFilmViewModel(masterViewModel);
        }

        public bool CanLoadCheckAvailability
        {
            get { return (masterViewModel.EmployeeLogin != null && (masterViewModel.EmployeeRole.Equals("Manager") || masterViewModel.EmployeeRole.Equals("Seller"))); }
        }

        RelayCommand loadMakeAReservationCommand;
        public ICommand LoadMakeAReservationCommand
        {
            get
            {
                if (loadMakeAReservationCommand == null)
                {
                    loadMakeAReservationCommand = new RelayCommand(param => this.LoadMakeAReservation(),
                        param => this.CanLoadMakeAReservation);
                }
                return loadMakeAReservationCommand;
            }
        }

        public void LoadMakeAReservation()
        {
            masterViewModel.LoadedView = new ChooseFilmViewModel(masterViewModel);
        }

        public bool CanLoadMakeAReservation
        {
            get { return (masterViewModel.EmployeeLogin != null && (masterViewModel.EmployeeRole.Equals("Manager") || masterViewModel.EmployeeRole.Equals("Seller"))); }
        }

        RelayCommand loadAddMovieCommand;
        public ICommand LoadAddMovieCommand
        {
            get
            {
                if (loadAddMovieCommand == null)
                {
                    loadAddMovieCommand = new RelayCommand(param => this.LoadAddMovie(),
                        param => this.CanLoadAddMovie);
                }
                return loadAddMovieCommand;
            }
        }

        public void LoadAddMovie()
        {
            masterViewModel.LoadedView = new AddFilmViewModel(masterViewModel);
        }

        public bool CanLoadAddMovie
        {
            get { return (masterViewModel.EmployeeLogin != null && (masterViewModel.EmployeeRole.Equals("Manager"))); }
        }

        RelayCommand loadAddScreeningCommand;
        public ICommand LoadAddScreeningCommand
        {
            get
            {
                if (loadAddScreeningCommand == null)
                {
                    loadAddScreeningCommand = new RelayCommand(param => this.LoadAddScreening(),
                        param => this.CanLoadAddScreening);
                }
                return loadAddScreeningCommand;
            }
        }

        public void LoadAddScreening()
        {
            masterViewModel.LoadedView = new AddScreeningViewModel(masterViewModel);
        }

        public bool CanLoadAddScreening
        {
            get { return (masterViewModel.EmployeeLogin != null && (masterViewModel.EmployeeRole.Equals("Manager"))); }
        }
    }
}