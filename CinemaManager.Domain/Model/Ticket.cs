﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaManager.Domain.Model
{
    public class Ticket : Entity
    {

        public int Seat { get; set; }
        public bool Paid { get; set; }


        public virtual Screening Screening { get; set; }
        public virtual ApplicationUser Viewer { get; set; }
        public virtual ApplicationUser Employee { get; set; }

    }
}
