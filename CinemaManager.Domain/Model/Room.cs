﻿using System.Collections.Generic;

namespace CinemaManager.Domain.Model
{
    public class Room:Entity
    {

        public string Name { get; set; }
        public virtual ICollection<Screening> Screenings { get; set; }
    }
}
