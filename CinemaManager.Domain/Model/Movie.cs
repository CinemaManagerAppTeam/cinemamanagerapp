﻿using System;
using System.Collections.Generic;

namespace CinemaManager.Domain.Model
{
    public class Movie:Entity
    {

        public string Title { get; set; }
        public string Description { get; set; }
        public TimeSpan  Duration { get; set; }
        public virtual IList<Screening> Screenings { get; set; }

        public Movie()
        {
            this.Screenings = new List<Screening>();
        }
    }
}
