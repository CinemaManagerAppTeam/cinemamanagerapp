﻿using System.Data.Entity;
using CinemaManager.Domain.Model;
using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CinemaManager.Domain.Data
{
    public class CinemaManagerData : IdentityDbContext<ApplicationUser>
    {
        static CinemaManagerData() 
        {
            Database.SetInitializer<CinemaManagerData>(null);
        }

        public CinemaManagerData() : base("CinemaManagerDatabase")
        {
            Database.SetInitializer<CinemaManagerData>(null);
        }


        public IDbSet<Movie> Movies { get; set; }
        public IDbSet<Room> Rooms { get; set; }
        public IDbSet<Screening> Screenings { get; set; }
        public IDbSet<Ticket> Tickets { get; set; }

        private readonly Lazy<CinemaManagerRepository> _cinemaManagerRepo;

        public ICinemaManagerRepository CinemaManager
        {
            get { return _cinemaManagerRepo.Value; }
        }
        public static CinemaManagerData Create()
        {
            return new CinemaManagerData();
        }
    }
}