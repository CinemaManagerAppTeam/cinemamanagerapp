﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CinemaManager.Domain.Model;


namespace CinemaManager.Domain.Data
{
    public interface ICinemaManagerRepository
    {
        void AddMovie(String title, String description, TimeSpan duration);
    }


    public class CinemaManagerRepository : ICinemaManagerRepository
    {
        private readonly CinemaManagerData _entities;

        public CinemaManagerRepository(CinemaManagerData entities)
        {
            _entities = entities;
        }

        public void AddMovie(String title, String description, TimeSpan duration)
        {
            _entities.Movies.Add(new Movie() { Title = title, Description = description, Duration = duration });
            _entities.SaveChanges();
        }

        public void AddScreening(string movie_title, DateTime datetime, string room_name)
        {
            _entities.Screenings.Add(new Screening() { Movie = GetMovieFromTitle(movie_title), DateTimeStart = datetime, Room = GetRoomFromName(room_name) });
            _entities.SaveChanges();
        }

        public List<Screening> GetScreeningsForDate(DateTime date)
        {
            return _entities.Screenings.Where(n => n.DateTimeStart.Day == date.Day && n.DateTimeStart.Month == date.Month && n.DateTimeStart.Year == date.Year).ToList();
        }

        public List<Screening> GetScreeningsForRoom(Room room)
        {
            return _entities.Screenings.Where(scr => scr.Room.Id == room.Id).ToList();
        }

        //f
        public List<Screening> GetScreeningsForRoomId(int id)
        {
            return _entities.Screenings.Where(scr => scr.Room.Id == id).ToList();
        }

        public Screening GetScreening(int id)
        {
            return _entities.Screenings.Where(screening => screening.Id == id).FirstOrDefault();
        }

        //f
        public List<Screening> GetScreenings()
        {
            return _entities.Screenings.ToList();
        }


        public List<string> GetMoviesTitles()
        {
            return _entities.Movies.Select(movie => movie.Title).ToList();
        }

        //f
        public List<Movie> GetMovies()
        {
            return _entities.Movies.ToList();
        }

        public List<string> GetRoomsNames()
        {
            return _entities.Rooms.Select(room => room.Name).ToList();
        }

        public Movie GetMovieFromTitle(string title)
        {
            return _entities.Movies.Where(movie => movie.Title == title).Single();
        }

        public Room GetRoomFromName(string name)
        {
            return _entities.Rooms.Where(room => room.Name == name).Single();
        }

        public List<int> GetTicketsReservedForScreening(string title, DateTime start, string room_name)
        {
            var room = GetRoomFromName(room_name);
            var movie = GetMovieFromTitle(title);

            var screening =
                movie.Screenings.FirstOrDefault((scr => scr.DateTimeStart.Equals(start) && scr.Room.Id.Equals(room.Id)));
            if (screening == null || screening.Tickets == null) return null;
            return screening.Tickets.Select(tick => tick.Seat).ToList();
        }

        public void ReserveTickets(List<int> tickets, string title, DateTime start, string room_name)
        {
            var room = GetRoomFromName(room_name);
            var movie = GetMovieFromTitle(title);
            var screening =
                movie.Screenings.FirstOrDefault((scr => scr.DateTimeStart.Equals(start) && scr.Room.Id.Equals(room.Id)));
            if (screening == null) return;

            foreach (var ticket in tickets)
            {
                _entities.Tickets.Add(new Ticket() { Paid = true, Screening = screening, Seat = ticket, });
            }
            _entities.SaveChanges();
        }
    }
}
