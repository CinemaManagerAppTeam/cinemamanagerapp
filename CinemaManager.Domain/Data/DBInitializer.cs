﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CinemaManager.Domain.Model;

namespace CinemaManager.Domain.Data
{
    class DBInitializer : DropCreateDatabaseIfModelChanges<CinemaManagerData>
    {
        protected override void Seed(CinemaManagerData context)
        {
            base.Seed(context);
            context.Database.Initialize(false);
        }
    }
}
