﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CinemaManager.Domain.Data
{
    public class Validation
    {
        private CinemaManagerData _entities;

        public Validation()
        {
            _entities = new CinemaManagerData();
        }


        public string ValidateAddMovie(string title, string description, string duration)
        {
            StringBuilder sb = new StringBuilder();

            if (_entities.Movies.Where(m => m.Title == title).Count() != 0)
                sb.Append("Movie already exists!");

            if (title == "" || description == "" || duration == "")          
                sb.Append("\nAll fields are required!");

            TimeSpan b = new TimeSpan();
            if (TimeSpan.TryParse(duration, out b))
            {
                TimeSpan a = TimeSpan.Parse(duration);
                if (a <= new TimeSpan(0, 0, 0))
                    sb.Append("\nProvide duration in format: HH:MM:SS!");
                if (a.Days != 0 || a.Milliseconds != 0)
                    sb.Append("\nProvide duration in format: HH:MM:SS!");
            }
            else
            {
                sb.Append("\nProvide duration in format: HH:MM:SS!");
            }

            var message = sb.ToString();
            if (message.Length > 0) return message;
            else return null;

        }

        public string ValidateAddScreening(string title, DateTime datetime, string room_name)
        {
            StringBuilder sb = new StringBuilder();

            if (title == null || room_name == null)
            {
                sb.Append("\nAll fields are required!");
                return sb.ToString();
            }

            if (datetime < DateTime.Now)
                sb.Append("Cannot add screening - start time of screening already passed!");

            var repo = new CinemaManagerRepository(new CinemaManagerData()) ;
            var movie = repo.GetMovieFromTitle(title);
            var room = repo.GetRoomFromName(room_name);

            var screenings = repo.GetScreeningsForRoom(room);

            foreach (var screening in screenings)
            {
                if (screening.DateTimeStart + screening.Movie.Duration > datetime && datetime + movie.Duration > screening.DateTimeStart)
                    sb.Append(string.Format("\nCannot add screening - movie {0} in room {1} begins at {2}", screening.Movie.Title, screening.Room.Name, screening.DateTimeStart ));
            }


            var message = sb.ToString();
            if (message.Length > 0) return message;
            else return null;
        }


        public string ValidateChooseFilm(DateTime datetime, string role)
        {
            StringBuilder sb = new StringBuilder();
            if (datetime < DateTime.Now)
                sb.Append("Cannot choose film - start time of film already passed!");
            if (role == "Seller" && datetime - DateTime.Now > new TimeSpan(7, 0, 0, 0))
                sb.Append("Cannot choose film - for a seller no more than 7 days ahead!");

            var message = sb.ToString();
            if (message.Length > 0) return message;
            else return null;
        }


    }
}
