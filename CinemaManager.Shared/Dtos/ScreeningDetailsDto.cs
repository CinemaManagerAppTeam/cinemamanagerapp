﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaManager.Shared.Dtos
{
    public class ScreeningDetailsDto
    {
        public DateTime DateTimeStart { get; set; }
        public int ScreeningId { get; set; }

        //from Movie
        public string Title { get; set; }
        public string Description { get; set; }
        public TimeSpan Duration { get; set; }
        public int MovieId { get; set; }

        //from Room
        public string RoomName { get; set; }
        public int RoomId { get; set; }

    }
}
