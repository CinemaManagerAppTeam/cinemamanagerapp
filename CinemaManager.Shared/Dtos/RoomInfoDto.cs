﻿namespace CinemaManager.Shared.Dtos
{
    public class RoomInfoDto
    {
        public string RoomName { get; set; }
    }
}