﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaManager.Shared.Dtos
{
    public class RoomDetailsDto
    {
        public string Name { get; set; }
        public int RoomId { get; set; }
    }
}
