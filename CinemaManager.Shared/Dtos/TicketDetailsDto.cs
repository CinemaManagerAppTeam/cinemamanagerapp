﻿using System;

namespace CinemaManager.Shared.Dtos
{
    public class TicketDetailsDto
    {
        public int SeatNumber { get; set; }
        public string UserLogin { get; set; }
        public int ScreeningId { get; set; }
    }
}