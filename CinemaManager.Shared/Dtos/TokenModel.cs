﻿using Newtonsoft.Json;

namespace CinemaManager.Shared.Dtos
{
    namespace CinemaManager.Shared.Dtos
    {
        public class TokenModel
        {
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }

            [JsonProperty(".expires")]
            public string ExpiresAt { get; set; }

            [JsonProperty("expires_in")]
            public int ExpiresIn { get; set; }

            [JsonProperty(".issued")]
            public string IssuedAt { get; set; }

            [JsonProperty("token_type")]
            public string TokenType { get; set; }

            [JsonProperty("userName")]
            public string Username { get; set; }
        }

    }
}