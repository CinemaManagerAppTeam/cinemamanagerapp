﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaManager.Shared.Dtos
{
    public class MovieDetailsDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Duration { get; set; }
        public int MovieId { get; set; }
    }
}
